# MeetYourPeufien
=====

1. Cloner le projet en utilisant la commande `git clone https://github.com/resourcepool/formation-spring-boot`

2. Importer le projet dans IntelliJ IDEA en important le fichier "pom.xml" à la racine de ce repository.

3. Exécuter la DB mysql. Si vous avez docker, vous pouvez utiliser la commande suivante:
```
docker run --name mariadb --rm -e MYSQL_ROOT_PASSWORD=toor -e MYSQL_DATABASE=defaultdb -p 3306:3306 -v "`pwd`/initdb:/docker-entrypoint-initdb.d" mariadb
```

4. Exécuter les deux scripts sql dans l'ordre (1. Tables, 2. Default Entries) contenus dans le dossier initdb.

5. Connecter vous sur `http://localhost:8080/` pour accéder au site.

6. Créer vous un compte ou accéder à un compte déjà existant à l'aide d'une adresse mail:
```
paul.harrohide@epfedu.fr
harry.covert@epfedu.fr
alain.posteur@epfedu.fr
elvire.debord@epfedu.fr
laurent.barre@epfedu.fr
homer.cipourtoux@epfedu.fr
gaston.laplouz@epfedu.fr
gisele.detable@epfedu.fr
thomas.ouaque@epfedu.fr
sacha.telfrize@epfedu.fr
alphonse.danlmur@epfedu.fr
barack.afritt@epfedu.fr
camille.onette@epfedu.fr
clement.tine@epfedu.fr
elie.coptere@epfedu.fr
hakim.embien@epfedu.fr
jean.nemard@epfedu.fr
jean.rigole@epfedu.fr
jessica.niche@epfedu.fr
jules.hule@epfedu.fr
justin.ptipeu@epfedu.fr
kelly.diote@epfedu.fr
lara.tatouille@epfedu.fr
laure.edubois@epfedu.fr
medhi.khaman@epfedu.fr
nicolas.nihorangina@epfedu.fr
otto.graf@epfedu.fr
paul.hemique@epfedu.fr
paul.ochon@epfedu.fr
paula.gratte@epfedu.fr
pit.za@epfedu.fr
remy.fasol@epfedu.fr
rose.devan@epfedu.fr
sarah.frechit@epfedu.fr
scott.che@epfedu.fr
tom.egerie@epfedu.fr
vicky.nepamore@epfedu.fr
yves.heron@epfedu.fr
```

7. Vous êtes désormais connecté. Vous pouvez à tout moment modifier vos données personnelles ou bien supprimer votre profil en suivant la navigation dans la colonne de gauche.

8. A droite se situent deux tableaux récapitulatifs des scores de tous les utilisateurs (Top10 et Worst10). Pour accéder au classement complet, cliquer sur le bouton "classement" en haut à gauche de votre fenêtre.

9. Au milieu de la page se situe l'interface de match. 4 boutons sont présents pour que vous puissiez donner votre avis sur la personne à noter:
- pierre (-1): même pas en rêve.
- bierre (+1): en étant saoul, ça peut passer.
- pêche (+1): pour une relation sans prise de tête, pourquoi pas.
- bague (+3): épouse-moi !

En cas de réponses similaires entre deux utilisateurs, un match se crée. Retrouvez tous vos matchs en bas de la colonne de gauche !


A NOTER QUE CETTE APPLICATION EST PUREMENT FICTIVE ET QU'EN AUCUN CAS ELLE NE DOIT ÊTRE PRISE AU SÉRIEUX !
