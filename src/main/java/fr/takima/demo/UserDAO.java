package fr.takima.demo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 *
 */
@Repository
public interface UserDAO extends CrudRepository<User, Long> {
    Iterable<User> findTop10ByOrderByGradeDesc();
    Iterable<User> findFirst10ByOrderByGradeAsc();
    Iterable<User> findAllByOrderByGradeDesc();
    User findByEmail(String email);
    User findById(int id);
    List<User> findBySex(int sex);
}
