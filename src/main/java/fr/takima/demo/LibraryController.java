package fr.takima.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import fr.takima.demo.storage.StorageService;


/**
 *
 */
@RequestMapping("/")
@Controller
public class LibraryController {

    private final UserDAO userDAO;
    private final MatchDAO matchDAO;
    private StorageService storageService;
    private String nameFile;
    private User modifiedUser;


    public LibraryController(UserDAO userDAO, MatchDAO matchDAO, StorageService storageService) {
        this.userDAO = userDAO;
        this.matchDAO = matchDAO;
        this.storageService = storageService;
        this.nameFile = "";
        this.modifiedUser = null;
    }

    @GetMapping
    public String homePage(Model m) {
        m.addAttribute("user", new User());
        return "login";
    }

    @GetMapping("/new")
    public String addUserPage(Model m) {
        m.addAttribute("user", new User());
        return "new";
    }

    @PostMapping("/new")
    public RedirectView createNewUser(@ModelAttribute User user, @RequestParam("picture") MultipartFile picture) {

        this.nameFile = storageService.store(picture);

        user.setDrunk(0);
        user.setRock(0);
        user.setPeach(0);
        user.setLove(0);
        user.setGrade(0);
        user.setPhoto(this.nameFile);
        userDAO.save(user);

        return new RedirectView("/user/" + user.getId());
    }

    @RequestMapping("/image/{imageName}")
    @ResponseBody
    public byte[] getImage(@PathVariable(value = "imageName") String imageName) throws IOException {
        return Files.readAllBytes(storageService.load(imageName));
    }

    @GetMapping("/listfiles")
    public String listAllFiles(Model model) {
        model.addAttribute("image", nameFile);
        return "listFiles";
    }

    @GetMapping("/login")
    public String LoginPage(Model m) {
        m.addAttribute("user", new User());
        m.addAttribute("toast", "");
        return "login";
    }

    @GetMapping("/login/error")
    public String LoginPageError(Model m) {
        m.addAttribute("user", new User());
        m.addAttribute("toast", "Aucun compte associé à ce mail");
        return "login";
    }

    @PostMapping("/login")
    public RedirectView LoginUser(@ModelAttribute User user, Model m) {
        User connectedUser = userDAO.findByEmail(user.getEmail());
        if(connectedUser == null){
            return new RedirectView("/login/error");
        }
        return new RedirectView("/user/" + connectedUser.getId());
    }

    @GetMapping("/user/{userID}")
    public String userPage(@PathVariable(value = "userID") int id, Model m) {
        User connectedUser = userDAO.findById(id);

        List<User> lovableUsers;
        if (connectedUser.getOrientation() == 2) {
            lovableUsers = (List<User>) userDAO.findAll();
        } else {
            lovableUsers = userDAO.findBySex(connectedUser.getOrientation());
        }

        m.addAttribute("connectedUser", connectedUser);
        m.addAttribute("allUsers", userDAO.findAllByOrderByGradeDesc());
        m.addAttribute("targetUser", connectedUser.getMatchableUser(lovableUsers));
        m.addAttribute("topUsers", userDAO.findTop10ByOrderByGradeDesc());
        m.addAttribute("worstUsers", userDAO.findFirst10ByOrderByGradeAsc());
        m.addAttribute("matchedUsers", connectedUser.getMatchedUsers());
        return "userProfile";
    }


    @PostMapping("/user/{userID}/{userVote}/{targetUserID}")
    public RedirectView MatchUser(@PathVariable(value = "userID") int id, @PathVariable(value = "userVote") int userVote, @PathVariable(value = "targetUserID") int targetUserID) {
        User targetUser = userDAO.findById(targetUserID);
        userDAO.save(targetUser.upgradeUserGrade(userVote));
        Match match = new Match(null, userVote, userDAO.findById(id), targetUser);
        matchDAO.save(match);
        return new RedirectView("/user/" + id);
    }

    @GetMapping("/user/{userID}/classement")
    public String rankingPage(@PathVariable(value = "userID") int id, Model m) {
        m.addAttribute("connectedUser", userDAO.findById(id));
        m.addAttribute("allUsers", userDAO.findAllByOrderByGradeDesc());
        return "classement";
    }

    @PostMapping("/user/{userID}/classement")
    public RedirectView userPage(@PathVariable(value = "userID") int id) {
        return new RedirectView("/user/" + id);
    }


    @GetMapping("/user/{userID}/modification")
    public String updatePage(Model m, @PathVariable(value = "userID") int id) {
        m.addAttribute("connectedUser", userDAO.findById(id));
        this.modifiedUser = this.userDAO.findById(id);
        return "modification";
    }

    @PostMapping("/user/{userID}/modification")
    public RedirectView updateUser(@ModelAttribute User connectedUser, @RequestParam("picture") MultipartFile picture) {
        if (picture.getSize() == 0) {
            connectedUser.setPhoto(this.modifiedUser.getPhoto());
        } else {
            this.nameFile = storageService.store(picture);
            connectedUser.setPhoto(this.nameFile);
        }

        userDAO.save(connectedUser);
        return new RedirectView("/user/" + connectedUser.getId());
    }

    @GetMapping("/user/{userID}/delete")
    public RedirectView deleteUser(@PathVariable(value = "userID") int id) {
        userDAO.delete(userDAO.findById(id));
        return new RedirectView("/login");
    }

}
