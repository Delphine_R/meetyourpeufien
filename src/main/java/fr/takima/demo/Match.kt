package fr.takima.demo

import javax.persistence.*

@Entity(name = "matches")
data class Match(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id var id: Int?,
        @Column(name = "match_type") var matchType: Int?,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "wanted_match")
        var wantedMatch: User?,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "target_match")
        var targetMatch: User?){

    constructor() : this(null, 0, null, null)


        override fun toString(): String {
                return "Match(id=$id, matchType=$matchType)"
        }


}