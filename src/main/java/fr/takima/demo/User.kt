package fr.takima.demo

import java.util.*
import javax.persistence.*

/**
 *
 */
@Entity(name = "users")
data class User (
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id var id: Int?,
        @Column(name = "first_name") var firstName: String?,
        @Column(name = "last_name") var lastName: String?,
        @Column(name = "age") var age: Int?,
        @Column(name = "sex") var sex: Int?,
        @Column(name = "orientation") var orientation: Int?,
        @Column(name = "rock") var rock: Int?,
        @Column(name = "drunk") var drunk: Int?,
        @Column(name = "peach") var peach: Int?,
        @Column(name = "love") var love: Int?,
        var grade: Int?,
        @Column(name = "email") var email: String?,
        @Column(name = "promo") var promo: Int?,
        @Column(name = "photo") var photo: String?,
        @Column(name = "description") var description: String?,

        @OneToMany(mappedBy = "wantedMatch", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
        var wantedMatch:List<Match>?,

        @OneToMany(mappedBy = "targetMatch", cascade = arrayOf(CascadeType.ALL), fetch = FetchType.LAZY)
        var targetMatch:List<Match>?){

    constructor() : this(null, null, null, null, 0, 0,0,0,0,0, 0,null, 2020, null, null, null, null)

    override fun toString(): String {
        return "User(id=$id, firstName=$firstName, lastName=$lastName)"
    }

    fun upgradeUserGrade(userVote: Int): User {
        when (userVote) {
            -1 -> rock = rock!! + 1
            1 -> drunk = drunk!! + 1
            2 -> peach = peach!! + 1
            3 -> love = love!! + 1
            else -> {
            }
        }
        grade = grade!! + userVote
        return this;
    }

    fun getMatchableUser(lovableUsers: MutableList<User>): User {
        for (match in wantedMatch!!) {
            lovableUsers.remove(match.targetMatch)
        }

        lovableUsers.remove(this)
        lovableUsers.shuffle()

        return if (lovableUsers.isNotEmpty()) {
            lovableUsers[0]
        } else User()
    }

    fun getMatchedUsers(): List<User> {

        //val targetingMatches = matchDAO.findByTargetMatch(user) // Tous les matchs de gens vers l'user
        //val userMatches = matchDAO.findByWantedMatch(user) // Tous les matchs crées par l'user

        val matchedUsers = ArrayList<User>()

        for (targetingMatch in targetMatch!!) {
            for (userMatch in wantedMatch!!) {
                if (userMatch.targetMatch == targetingMatch.wantedMatch && userMatch.matchType == targetingMatch.matchType) {
                    matchedUsers.add(userMatch.targetMatch!!)
                    break
                }
            }
        }
        return matchedUsers
    }

}
