package fr.takima.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 *
 */
@Repository
public interface MatchDAO extends CrudRepository<Match, Long> {
    List<Match> findByWantedMatch(User user);
    List<Match> findByTargetMatch(User user);
}