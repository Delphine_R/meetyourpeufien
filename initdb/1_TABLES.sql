create table users
(
    id int auto_increment,
    constraint users_pk
        primary key (id),
    first_name TEXT not null,
    last_name TEXT not null,
    age int null,
    sex int null,
    orientation int null,
    rock int null,
    drunk int null,
    peach int null,
    love int null,
    grade int null,
    email TEXT null,
    promo int null,
    photo TEXT null,
    description TEXT null
);

create table matches
(
    id int auto_increment,
    constraint matches_pk
        primary key (id),
    match_type int null,
    wanted_match int null,
    target_match int null
);
